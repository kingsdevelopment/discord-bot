// dependency vars
var winston = require('winston')
var fs = require('fs')
var ini = require('ini')
var Discordbot = require('discord.io');

//shared
log = new (winston.Logger)({ transports: [new (winston.transports.File)({name: 'info-file', filename: 'logs/info.log', level: 'info' }), new (winston.transports.File)({ name: 'error-file', filename: 'logs/error.log', level: 'error' }), new (winston.transports.File)({ name: 'debug-file', filename: 'logs/debug.log', level: 'debug' }) ] })
config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'))

bot = new Discordbot({ email: config.Discord.email, password: config.Discord.password, autorun: true });
commands = require('./libs/commands');
m = require('./libs/message');


//run
fs.stat('config.ini', function(err, stat) {
    if(err == null) {
        //start bot
        events = require('./libs/events');        
    } else {
        console.log('Please rename config.ini.example to config.ini and fill out the configurations');
    }
});


//show errors
process.on('uncaughtException', function (error) {
   console.log(error.stack);
});