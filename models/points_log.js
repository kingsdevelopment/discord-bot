var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('points_log', {
        user_id: {
            type: Sequelize.STRING
        },
        receiver_id: {
            type: Sequelize.STRING
        },
        created: { 
            field: 'created_at',
            type: Sequelize.DATE, 
            defaultValue: Sequelize.NOW
        },
        server_id: { 
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.ENUM('user', 'daily', 'achievement', 'kick', 'ban')
        }
}, {
    tableName: 'points_log'
});