var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('silent_channel', {
        channel_id: {
            type: Sequelize.STRING
        }
}, {
    tableName: 'silent_channels'
});