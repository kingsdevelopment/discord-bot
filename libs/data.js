var async = require('async')
var moment = require('moment');

var db = require('./models')

var user = db.user;
var server = db.server;
var command_log = db.command_log;
var silent_channel = db.silent_channel;
var points_log = db.points_log
var chat_log = db.chat_log
var achievement = db.achievement

var addUserPoints = require('../commands/points').addUserPoints;
var getUserPoints = require('../commands/points').getUserPoints;

function checkUser(botuser, cb)
{
    if(botuser.user)
    {
        var username = botuser.user.username
        var userid = botuser.user.id

        user.findOrCreate({where: {user_id: userid}, defaults: {username: username, coins: 100, status: 0}}).then(function(user) {
            user = user[0]

            if(user.username != username)
            {
                user.username = username
                user.save().then(function(){
                    cb(user)
                })
            }
            else
            {
                cb(user)
            }
        })
    }        
}

function checkServer(botserver, cb)
{
    var servername = botserver.name
    var serverid = botserver.id

    server.findOrCreate({where: {server_id: serverid}, defaults: {name: servername}}).then(function(server) {
        cb(server[0]);
    })
}
    

function checkServerUser(user, server, cb)
{
    server.getUsers({where: {id: user.id}}).then(function(users){
        if(users.length == 0)
        {
            server.addUser(user);

            if(typeof cb == 'function')
            {
                cb();
            }
        }
    });
}

function checkDailyPoints(user, server, channel_id, cb)
{
    day_ago = moment.utc().subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss');

    points_log.find({
        order: [
            // Will escape username and validate DESC against a list of valid direction parameters
            ['created_at', 'DESC'],
        ],
        where: {
            receiver_id: user.user_id,
            server_id: bot.serverFromChannel(channel_id),
            type: 'daily'
        }
    }).then(function(log){
        if(log && user.user_id != bot.id)
        {
            difference = moment().diff(moment(log.created), 'days');
            difference = difference; //make it not a minus.

            if(difference >= 5)
            {
                deductPts = (difference-4)*-1;
                addUserPoints(bot.id, user.user_id, channel_id, deductPts, false, 'daily', function(amount){
                    m.sendMessage(channel_id, '<@' + user.user_id + '> has lost ' + deductPts*-1 + ' point(s) for being inactive for ' + difference + ' day(s) and now has ' + amount + ' points!')
                });
            }

            if(difference > 0 && difference < 5)
            {
                addUserPoints(bot.id, user.user_id, channel_id, 1, false, 'daily', function(amount){
                    m.sendMessage(channel_id, '<@' + user.user_id + '> has received a daily point and now has ' + amount + ' points!')
                });
            }
        }
        else if(user.user_id != bot.id)
        {
            addUserPoints(bot.id, user.user_id, channel_id, 1, false, 'daily', function(amount){
                m.sendMessage(channel_id, '<@' + user.user_id + '> has joined and earned his/her first point!')
            });
        }
    });
}


function checkAchievements(user, server, channel_id, cb)
{
    //get all achievements by the server
    achievement.findAll({
        where : {
            server_id: server.id
        }
    }).then(function(achievements){
        if(user.user_id != bot.id)
        {
            // loop through them..
            async.eachLimit(achievements, 1, function(s_achievement, nextAchievement){

                //check if user has achievement
                user.hasAchievement(s_achievement).then(function(gotAchievement){
                    if(!gotAchievement)
                    {
                        //user doesn't have achievement, let's check if it applies.

                        if(s_achievement.type == 'messages')
                        {
                            //let's see how many messages the user has.
                            user.countMessages({
                                where: {
                                    server_id: server.server_id
                                }
                            }).then(function(messageCount){
                                if(messageCount >= parseInt(s_achievement.behavior))
                                {
                                    user.addAchievement(s_achievement);

                                    addUserPoints(bot.id, user.user_id, channel_id, s_achievement.points, false, 'achievement', function(amount){
                                        m.sendMessage(channel_id, '<@' + user.user_id + '> just completed the "' + s_achievement.description + '" achievement and received ' + s_achievement.points + ' points, he/she now has ' + amount + ' points!')
                                    });
                                    
                                }

                                nextAchievement()
                            });
                        }

                        if(s_achievement.type == 'points')
                        {
                            getUserPoints(user.user_id, channel_id, function(pts){
                                if(pts.amount >= parseInt(s_achievement.behavior))
                                {
                                    user.addAchievement(s_achievement);

                                    addUserPoints(bot.id, user.user_id, channel_id, s_achievement.points, false, 'achievement', function(amount){
                                        m.sendMessage(channel_id, '<@' + user.user_id + '> just completed the "' + s_achievement.description + '" achievement and received ' + s_achievement.points + ' points, he/she now has ' + amount + ' points!')
                                    });
                                }

                                nextAchievement()
                            })
                        }
                    }
                    else
                    {
                        nextAchievement();
                    }
                    

                })
            });
        }
    });
}


function checkData(userid, channelid, message)
{
    var privateMessage = m.isPrivate(channelid)

    if(privateMessage)
    {
        var botuser = {user: privateMessage.recipient}
    }
    else
    {
        serverid = bot.serverFromChannel(channelid)
        var botserver = bot.servers[serverid]
        var botuser = botserver.members[userid]
    }

    if(!botuser)
    {
        return false;
    }

    checkUser(botuser, function(user){
        if(!privateMessage)
        {
            checkServer(botserver, function(server){

                checkServerUser(user, server)

                if(server.enable_points)
                {
                    checkDailyPoints(user, server, channelid)
                    checkAchievements(user, server, channelid)    
                }
                
            })
        }

        logChat(user.id, channelid, message)
    })


} 

function getSilentChannels(cb)
{
    var silented = [];
    silent_channel.findAll().then(function(channels){
        
        async.each(channels, function(channel){
            silented.push(channel.channel_id);
        });
        cb(silented);
    });
}

function logCommand(userID, channelID, command, parameters)
{
    var privateMessage = m.isPrivate(channelID)

    var server_id = (!privateMessage ? bot.serverFromChannel(channelID) : 0)

    command_log.create({
        user_id: userID,
        server_id: server_id,
        channel_id: channelID,
        command: command.name,
        parameters: parameters.join(' ')
    });
}

function logChat(userID, channelID, message)
{
    var privateMessage = m.isPrivate(channelID)

    var server_id = (!privateMessage ? bot.serverFromChannel(channelID) : 0)

    chat_log.create({
        user_id: userID,
        server_id: server_id,
        channel_id: channelID,
        message: message
    });
}

module.exports = {
    checkData: checkData,
    getSilentChannels: getSilentChannels,
    logCommand: logCommand,
    logChat: logChat
}