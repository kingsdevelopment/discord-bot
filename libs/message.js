var S = require('string');

var async = require('async')
var isAdmin = require('../libs/roles').isAdmin;
var data = require('./data')

var getServer = require('../commands/commands').getServer

var botConfig = config.Bot


/*Function declaration area*/
function sendMessage(ID, message, cb) {
    bot.sendMessage({
        to: ID,
        message: message
    }, function(response) {
        //console.log(response);
    });

    log.debug("Send message: " + message + " in " + ID)

    if(typeof cb == 'function')
    {
        cb();
    } 
}

function checkParameters(message)
{
    parameters = [];
    async.forEach(message.match(/(".*?"|[^"\s]+)+(?=\s*|\s*$)/g), function(parameter){
        parameters.push(parameter.replace(/"/g, ''));
    })

    return parameters
}

function runCommand(user, userID, channelID, message, rawEvent)
{
    parameters = checkParameters(message)
    command = parameters[0].replace(botConfig.command_prefix, '').toLowerCase()


    if(userID != bot.id)
    {
        if(command in commands)
        {
            parameters.splice(0, 1)
            command = commands[command]
            if(command.serverOnly && m.isPrivate(channelID))
            {
                sendMessage(channelID, 'Sorry, you can\'t use this command in a private message conversation');
            }
            else
            {
                if(m.isPrivate(channelID))
                {
                    if(!command.adminOnly || command.adminOnly && isAdmin(user, userID, channelID))
                    {
                        if(command.fixMessage)
                        {
                            message = bot.fixMessage(message)
                        }

                        if(command.minParameters > parameters.length)
                        {
                            sendMessage(channelID, 'command "' + command.name + '" requires atleast ' + command.minParameters + ' parameters!\nFor more info run: ' + botConfig.command_prefix + "help " + command.name)
                        }
                        else
                        {
                            if(command.bossOnly)
                            {
                                if(botConfig.bot_boss == userID)
                                {
                                    command.toRun(user, channelID, parameters, userID)
                                }
                                else
                                {
                                    sendMessage(channelID, "You sneaky little bastard!")
                                }
                            }
                            else
                            {
                                command.toRun(user, channelID, parameters, userID)
                            }

                            data.logCommand(userID, channelID, command, parameters);

                            log.info(user + " ran the command: " + command.name + " with the parameters: " + parameters.join(' '))
                        }
                    }
                    else
                    {
                        sendMessage(channelID, "You sneaky little bastard!")
                    }
                }
                else
                {
                    getServer(bot.serverFromChannel(channelID), function(cserver){
                        disabled = JSON.parse(cserver.disabled_commands)

                        if(disabled.indexOf(command.name) >= 0)
                        {
                            // return sendMessage(channelID, "Command `" + command.name + "` is disabled.")
                            return false
                        }

                        if(!command.adminOnly || command.adminOnly && isAdmin(user, userID, channelID))
                        {
                            if(command.fixMessage)
                            {
                                message = bot.fixMessage(message)
                            }

                            if(command.minParameters > parameters.length)
                            {
                                sendMessage(channelID, 'command "' + command.name + '" requires atleast ' + command.minParameters + ' parameters!\nFor more info run: ' + botConfig.command_prefix + "help " + command.name)
                            }
                            else
                            {
                                if(command.bossOnly)
                                {
                                    if(botConfig.bot_boss == userID)
                                    {
                                        command.toRun(user, channelID, parameters, userID)
                                    }
                                    else
                                    {
                                        sendMessage(channelID, "You sneaky little bastard!")
                                    }
                                }
                                else
                                {
                                    command.toRun(user, channelID, parameters, userID)
                                }

                                data.logCommand(userID, channelID, command, parameters);

                                log.info(user + " ran the command: " + command.name + " with the parameters: " + parameters.join(' '))
                            }
                        }
                        else
                        {
                            sendMessage(channelID, "You sneaky little bastard!")
                        }
                    })
                }
            }
        }
        else
        {
            if(!m.isPrivate(channelID))
            {
                getServer(bot.serverFromChannel(channelID), function(cserver){
                    disabled = JSON.parse(cserver.disabled_commands)
                    
                    if(/<@\d+>\+\+/g.test(message) || /<@\d+> \+\+/g.test(message))
                    {
                        command = commands['acknowledge']

                        if(disabled.indexOf(command.name) >= 0)
                        {
                            // return sendMessage(channelID, "Command `" + command.name + "` is disabled.")
                            return false
                        }

                        parameters[0].replace(/\+/g, '')
                        if(parameters.indexOf('++') > -1)
                        {
                            parameters.splice(parameters.indexOf('++'), 1)
                        }

                        if(parameters.length == 1)
                        {
                            parameters[1] = 1;
                            parameters[2] = 'custom';    
                        }

                        command.toRun(user, channelID, parameters, userID)
                    }

                    if(/<@\d+>\-\-/g.test(message) || /<@\d+> \-\-/g.test(message))
                    {
                        command = commands['acknowledge']

                        if(disabled.indexOf(command.name) >= 0)
                        {
                            // return sendMessage(channelID, "Command `" + command.name + "` is disabled.")
                            return false
                        }

                        parameters[0].replace(/\-/g, '')
                        if(parameters.indexOf('--') > -1)
                        {
                            parameters.splice(parameters.indexOf('--'), 1)   
                        }

                        if(parameters.length == 1)
                        {
                            parameters[1] = -1;
                            parameters[2] = 'custom';    
                        }
                        else if(parameters.length == 2)
                        {
                            parameters[2] = 'minus'
                        }

                        command.toRun(user, channelID, parameters, userID)
                    }
                });
            }
            

            if(S(message).startsWith('https://discord.gg/'))
            {
                if(m.isPrivate(channelID))
                {
                    command = commands['join']
                    command.toRun(user, channelID, m.checkParameters(message), userID)    
                }
            }

            if(S(message).startsWith('<@' + bot.id + '>'))
            {
                command = commands['chat']
                command.toRun(user, channelID, m.checkParameters(message), userID)  
            }


            // minus?
            // if(/<@\d+>\-\-/g.test(message) || /<@\d+> \-\-/g.test(message))
            // {
            //     command = commands['acknowledge']
            //     parameters[0].replace(/\+/g, '')
            //     if(parameters.indexOf('--') > -1)
            //     {
            //         parameters.splice(parameters.indexOf('--'), 1)   
            //     }

            //     command.toRun(user, channelID, parameters, userID)
            // }
        }
        // else
        // {
        //     sendMessage(channelID, 'command "' + command + '" doesn\'t exist!')
        // }
    }
}


function isPrivate(channelID)
{
    var isPrivateMessage = false;
    async.forEach(bot.directMessages, function(dm){
        if(dm.id == channelID)
        {
            isPrivateMessage = dm;
        }
    });

    return isPrivateMessage;
}


module.exports = {
    sendMessage:  sendMessage,
    checkParameters: checkParameters,
    runCommand: runCommand,
    isPrivate: isPrivate
}