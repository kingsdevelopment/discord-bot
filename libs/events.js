var S = require('string');

var botConfig = config.Bot
var owners = botConfig.owners

var vk = require('../commands/vote_kick');

var data = require('./data');

//bot events
bot.on("err", function(error) { log.error(error); console.log(error); /*console.log(JSON.parse(error))*/ });
bot.on("debug", function(rawEvent) { });

bot.on("ready", function(rawEvent) {
    console.log("Connected!");
    console.log("Logged in as: ");
    console.log(bot.username + " - (" + bot.id + ")");

    if(botConfig.debug)
    {
        console.log("DEBUGGING ENABLED")
    }

    m.sendMessage(botConfig.bot_boss, "I am back!")

    rawEvent.d.guilds.forEach(function(server){
        if(botConfig.run_server_messages && !debug)
        {
            m.sendMessage(server.id, "Hazebot is now live in: " + server.name + "\nRun " + botConfig.command_prefix + "commands for a list of commands.\n\nKind regards Hazebot\nhttp://hazedevelopment.com")
        }
    });
});

bot.on("message", function(user, userID, channelID, message, rawEvent) {
    data.checkData(userID, channelID, message)

    data.getSilentChannels(function(channels){
        if(channels.indexOf(channelID) == -1 || S(message).startsWith(botConfig.command_prefix + "listen"))
        {
            if(S(message).startsWith(botConfig.command_prefix) || /<@\d+>\+\+/g.test(message) || /<@\d+> \+\+/g.test(message) || /<@\d+>\-\-/g.test(message) || /<@\d+> \-\-/g.test(message) || S(message).startsWith('https://discord.gg/') || S(message).startsWith('<@' + bot.id + '>'))
            {
                m.runCommand(user, userID, channelID, message, rawEvent)
            }

            if(S(message.toLowerCase()).startsWith('ping'))
            {
                m.sendMessage(channelID, 'Pong!')
            }

            serverID = bot.serverFromChannel(channelID);
            if(vk.voteKicks[serverID] && userID != vk.voteKicks[serverID].user.id && userID != bot.id)
            {

                if(!vk.voteKicks[serverID].voters[userID])
                {
                    if(message.toLowerCase() == 'y' || message.toLowerCase() == 'yes' || message.toLowerCase() == 'n' || message.toLowerCase() == 'no')
                    {
                        voteKickMessage = '';
                        if(message.toLowerCase() == 'y' || message.toLowerCase() == 'yes')
                        {
                            vk.voteKicks[serverID].voters[userID] = 'y';
                            voteKickMessage += user + ' voted **yes** to kick: ' + vk.voteKicks[serverID].user.user.username;
                        }
                        else if(message.toLowerCase() == 'n' || message.toLowerCase() == 'no')
                        {
                            vk.voteKicks[serverID].voters[userID] = 'n';
                            voteKickMessage += user + ' voted **no** to kick: ' + vk.voteKicks[serverID].user.user.username;
                        }


                        votedPercentage = (vk.getVotedUsers(serverID)/Object.keys(vk.voteKicks[serverID].voters).length)*100;
                        voteKickMessage += '\n'
                        voteKickMessage += 'Users voted: ' + vk.getVotedUsers(serverID) + '/' + Object.keys(vk.voteKicks[serverID].voters).length + ' (' + parseFloat(votedPercentage).toFixed(0) + '%)';

                        m.sendMessage(channelID, voteKickMessage, function(){
                            vk.checkVoteKick(serverID, channelID);
                        });

                    }
                    
                }
            }
        }
    });
    
});

bot.on("presence", function(user, userID, status, rawEvent) {
    if(status == 'online' && botConfig.welcome_back == "true")
    {
        channelID = rawEvent.d.guild_id;
        m.sendMessage(channelID, "Welcome back " + user + "!")
    }
});

bot.on("disconnected", function() {
    console.log("Bot disconnected");
    bot.connect() //Auto reconnect
});

module.exports = {}