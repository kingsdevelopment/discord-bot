var botConfig = config.Bot


//single
var sendInfo = require('../commands/info').command;
var sendCommands = require('../commands/commands').command;
var disable_command = require('../commands/commands').disable_command;
var enable_command = require('../commands/commands').enable_command;
var disabled_commands = require('../commands/commands').disabled_commands;
var searchAnime = require('../commands/anime').command;
var searchManga = require('../commands/manga').command;
var runGiphy = require('../commands/giphy').command;
var spam = require('../commands/spam').command;
var help = require('../commands/help').command;
var joinServer = require('../commands/join').command;
var leaveServer = require('../commands/leave').command;
var kickUser = require('../commands/kick').command;
var banUser = require('../commands/ban').command;
var killBot = require('../commands/kill').command;
var broadcast = require('../commands/broadcast').command;
var reddit = require('../commands/reddit').command;
var random = require('../commands/random').command;
var showAdmins = require('../commands/admins').command;
var deleteBotMessages = require('../commands/delete_messages').command;
var purge = require('../commands/delete_messages').purge;
var osu = require('../commands/ppy').command;
var rito = require('../commands/lol').command;
var voteKick = require('../commands/vote_kick').command;
var set_avatar = require('../commands/set_avatar').command;
var set_name = require('../commands/set_name').command;
var say = require('../commands/say').command;
var chat = require('../commands/chat').command;
var unlisten = require('../commands/unlisten').command;
var listen = require('../commands/listen').command;
var coins = require('../commands/coins').showCoins;
var giveCoins = require('../commands/coins').giveCoins;
var showPoints = require('../commands/points').showPoints;
var acknowledge = require('../commands/points').acknowledge;
var unfreeze_pts = require('../commands/points').unfreeze_pts;
var freeze_pts = require('../commands/points').freeze_pts;
var enable_points = require('../commands/points').enable_points;
var disable_points = require('../commands/points').disable_points;
var top_five = require('../commands/points').top_five;
var weather = require('../commands/forecast').weather;
var userinfo = require('../commands/user').userInfo;
var message_count = require('../commands/user').message_count;
var play = require('../commands/play').play;
var update = require('../commands/update').command;

//multi
var fun = require('../commands/fun');
var roles = require('../commands/roles');

var commands = {  
    'info':{  
        name:'info',
        adminOnly:false,
        toRun:sendInfo,
        minParameters:0,
        description:"Show info",
        instructions:"",
        bossOnly:false,
        serverOnly:false
    },
    'commands':{  
        name:'commands',
        adminOnly:false,
        toRun:sendCommands,
        minParameters:0,
        description:"Show a list of commands",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'anime':{  
        name:'anime',
        adminOnly:false,
        toRun:searchAnime,
        minParameters:1,
        description:"Search for the anime on hummingbird.me",
        instructions:"`<search_name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'manga':{  
        name:'manga',
        adminOnly:false,
        toRun:searchManga,
        minParameters:1,
        description:"Search for the manga on myanimelist.net",
        instructions:"`<search_name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'giphy':{  
        name:'giphy',
        adminOnly:false,
        toRun:runGiphy,
        minParameters:1,
        description:"Random giphy image with search tags",
        instructions:"`<search_name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'spam':{  
        name:'spam',
        adminOnly:true,
        toRun:spam,
        minParameters:2,
        description:"Spam a message for the amount provided.",
        instructions:"`<string>` `<amount>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'help':{  
        name:'help',
        adminOnly:false,
        toRun:help,
        minParameters:0,
        description:"Lookup instructions of other commands. ex. " + botConfig.command_prefix + "help `<command_name>`",
        instructions:"`<command_name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'join':{  
        name:'join',
        adminOnly:false,
        toRun:joinServer,
        minParameters:1,
        description:"Let the bot join a new server.",
        instructions:"`<invite_url>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'leave':{  
        name:'leave',
        adminOnly:true,
        toRun:leaveServer,
        minParameters:0,
        description:"Let the bot leave the server.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'kick':{  
        name:'kick',
        adminOnly:true,
        toRun:kickUser,
        minParameters:1,
        description:"Kick a user from the server.",
        instructions:"`<@username>`",
        fixMessage:false,
        bossOnly:false,
        serverOnly:true
    },
    'ban':{  
        name:'ban',
        adminOnly:true,
        toRun:banUser,
        minParameters:1,
        description:"Ban a user from the server.",
        instructions:"`<@username>` `<1 to 7 days (optional)[>`",
        fixMessage:false,
        bossOnly:false,
        serverOnly:true
    },
    'kill':{  
        name:'kill',
        adminOnly:false,
        toRun:killBot,
        minParameters:0,
        description:"Kill the bot.",
        instructions:"",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    },
    'broadcast':{  
        name:'broadcast',
        adminOnly:false,
        toRun:broadcast,
        minParameters:1,
        description:"Send message to all servers",
        instructions:"`<message>`",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    },
    'reddit':{  
        name:'reddit',
        adminOnly:false,
        toRun:reddit,
        minParameters:1,
        description:"Generate reddit url",
        instructions:"`<reddit_url>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'random':{  
        name:'random',
        adminOnly:false,
        toRun:random,
        minParameters:2,
        description:"Generate a random number between 2 numbers on random.org",
        instructions:"`<min>` `<max>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'admins':{  
        name:'admins',
        adminOnly:false,
        toRun:showAdmins,
        minParameters:0,
        description:"Return a list of admins.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'clean':{  
        name:'clean',
        adminOnly:true,
        toRun:deleteBotMessages,
        minParameters:0,
        description:"Delete all messages by the bot.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'ppy':{  
        name:'ppy',
        adminOnly:false,
        toRun:osu,
        minParameters:1,
        description:"Search for osu data",
        instructions:"`<username>` `<player|recent|top|replay(WIP) (optional)>` `<osu|taiko|mania|ctb (optional)>` `<beatmap_id (optional)>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'vote_kick':{  
        name:'vote_kick',
        adminOnly:false,
        toRun:voteKick,
        minParameters:1,
        description:"Vote kick a user",
        instructions:"`<@Username>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'lol':{  
        name:'lol',
        adminOnly:false,
        toRun:rito,
        minParameters:3,
        description:"Get data from league of legends.",
        instructions:"`<region>` summoner `<summoner name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'set_avatar':{  
        name:'set_avatar',
        adminOnly:false,
        toRun:set_avatar,
        minParameters:1,
        description:"Set bot avatar",
        instructions:"`<image url>`",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    },
    'set_name':{  
        name:'set_name',
        adminOnly:false,
        toRun:set_name,
        minParameters:1,
        description:"Set bot name",
        instructions:"`<new name>`",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    },
    'neko':{  
        name:'neko',
        adminOnly:false,
        toRun:fun.neko,
        minParameters:0,
        description:"Meow!",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'say':{  
        name:'say',
        adminOnly:true,
        toRun:say,
        minParameters:1,
        description:"Let the bot say stuff",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'chat':{  
        name:'chat',
        adminOnly:false,
        toRun:chat,
        minParameters:1,
        description:"Have a clever conversation with the bot.",
        instructions:"`<sentence>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'unlisten':{  
        name:'unlisten',
        adminOnly:true,
        toRun:unlisten,
        minParameters:0,
        description:"Disable the bot for the current channel.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'listen':{  
        name:'listen',
        adminOnly:true,
        toRun:listen,
        minParameters:0,
        description:"Re-enable the bot for the current channel.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'coins':{  
        name:'coins',
        adminOnly:false,
        toRun:coins,
        minParameters:0,
        description:"Show how much coins you have.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'give':{  
        name:'give',
        adminOnly:false,
        toRun:giveCoins,
        minParameters:2,
        description:"Send coins to another person.",
        instructions:"`<@user>` `<amount>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'points':{  
        name:'points',
        adminOnly:false,
        toRun:showPoints,
        minParameters:0,
        description:"View the user's points.",
        instructions:"`<@user (optional)>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'acknowledge':{  
        name:'acknowledge',
        adminOnly:false,
        toRun: acknowledge,
        minParameters:1,
        description:"Acknowledge a user.",
        instructions:"`<@user>` `<amount (admins)>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'unfreeze_pts':{  
        name:'unfreeze_pts',
        adminOnly:true,
        toRun: unfreeze_pts,
        minParameters:1,
        description:"Unfreeze a user's points.",
        instructions:"`<@user>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'freeze_pts':{  
        name:'freeze_pts',
        adminOnly:true,
        toRun: freeze_pts,
        minParameters:1,
        description:"Freeze a user's points.",
        instructions:"`<@user>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'enable_points':{  
        name:'enable_points',
        adminOnly:true,
        toRun: enable_points,
        minParameters:0,
        description:"Enable points system for the server.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'disable_points':{  
        name:'disable_points',
        adminOnly:true,
        toRun: disable_points,
        minParameters:0,
        description:"Disable points system for the server.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'weather':{  
        name:'weather',
        adminOnly:false,
        toRun: weather,
        minParameters:1,
        description:"Get current weather for a city.",
        instructions:"`<city> <metric|imperial (default: metric)>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'userinfo':{  
        name:'userinfo',
        adminOnly:false,
        toRun: userinfo,
        minParameters:0,
        description:"Get userinfo of yourself or another person.",
        instructions:"`<@user (optional)>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'create_role':{  
        name:'create_role',
        adminOnly:true,
        toRun: roles.createRole,
        minParameters:1,
        description:"Create a role.",
        instructions:"`<name> <color (default:default)>`\nAvailable colors: `" + roles.roleColors.join(', ').toLowerCase() + "`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'delete_role':{  
        name:'delete_role',
        adminOnly:true,
        toRun: roles.deleteRole,
        minParameters:1,
        description:"Delete a role.",
        instructions:"`<name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'add_to_role':{  
        name:'add_to_role',
        adminOnly:true,
        toRun: roles.addRole,
        minParameters:2,
        description:"Add role to a user.",
        instructions:"`<@user>` `<role name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'remove_from_role':{  
        name:'remove_from_role',
        adminOnly:true,
        toRun: roles.removeRole,
        minParameters:2,
        description:"Remove role from a user.",
        instructions:"`<@user>` `<role name>`",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'yomomma':{  
        name:'yomomma',
        adminOnly:false,
        toRun:fun.yoMomma,
        minParameters:0,
        description:"Bot will tell you a `yo momma` joke.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:false
    },
    'disable_command':{  
        name:'disable_command',
        adminOnly:true,
        toRun:disable_command,
        minParameters:1,
        description:"Disable a command for the server.",
        instructions:"<command_name>",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'enable_command':{  
        name:'enable_command',
        adminOnly:true,
        toRun:enable_command,
        minParameters:1,
        description:"Enable a command for the server.",
        instructions:"<command_name>",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'disabled_commands':{  
        name:'disabled_commands',
        adminOnly:false,
        toRun:disabled_commands,
        minParameters:0,
        description:"List all disabled commands.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'play':{  
        name:'play',
        adminOnly:false,
        toRun:play,
        minParameters:1,
        description:"Set bot game.",
        instructions:"<game name/id>",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    },
    'top_points':{  
        name:'top_points',
        adminOnly:false,
        toRun:top_five,
        minParameters:0,
        description:"Show the top 5 leading in points.",
        instructions:"",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'purge':{  
        name:'purge',
        adminOnly:true,
        toRun:purge,
        minParameters:2,
        description:"Delete messages of a user.",
        instructions:"<@user> <amount (max 25)>",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'messages':{  
        name:'messages',
        adminOnly:false,
        toRun:message_count,
        minParameters:0,
        description:"Show your message count.",
        instructions:"<@user (optional)>",
        fixMessage:true,
        bossOnly:false,
        serverOnly:true
    },
    'update':{  
        name:'update',
        adminOnly:false,
        toRun:update,
        minParameters:0,
        description:"Update the bot.",
        instructions:"<true|false>",
        fixMessage:true,
        bossOnly:true,
        serverOnly:false
    }
}

module.exports = commands