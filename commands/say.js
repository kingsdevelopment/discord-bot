function say(user, channelID, parameters, userID)
{
    m.sendMessage(channelID, parameters.join(' '));
}

module.exports = {
    command: say
}