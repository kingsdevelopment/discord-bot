var request = require('request');

//Giphy
function giphy(user, channelID, parameters, userID)
{
    searchString = parameters.join('+');
    var giphyUrl = 'http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=' + searchString.toLowerCase();
    request(giphyUrl, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            data = JSON.parse(body)
            data = data.data
            if(data.hasOwnProperty('image_url'))
            {
                returnImage = data.image_url
                m.sendMessage(channelID, returnImage)
            }
            else
            {
                m.sendMessage(channelID, 'No result for: "' + searchString.split('+').join(' ') + '".')
            }
        }
        else
        {
            m.sendMessage(channelID, 'No result for "' + searchString.split('+').join(' ') + '".')
            log.error('Failed to run giphy for: ' + giphyUrl + '\n' + error)
        }
    });
}

module.exports = {
    command: giphy
}