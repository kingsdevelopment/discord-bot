var botConfig = config.Bot
var async = require('async')

voteKicks = {};

function voteKick(user, channelID, parameters, userID)
{
    userID = parameters[0].replace('<@', '').replace('>', '')
    serverID = bot.serverFromChannel(channelID);

    if(userID == bot.id)
    {
        m.sendMessage(channelID, 'Did you just try to fucking kick me?\nhttps://media.giphy.com/media/1mjZ33k4qBCF2/giphy.gif');
        return false;
    }

    if(userID == botConfig.bot_boss)
    {
        m.sendMessage(channelID, 'Did you just try to fucking kick the BOSS?\nhttps://media.giphy.com/media/WVrBQAezwu8E0/giphy.gif');
        return false;
    }

    if(userID in bot.servers[serverID].members)
    {
        if(voteKicks[serverID])
        {
            m.sendMessage(channelID, 'There is already a vote kick on going for the user: ' + voteKicks[serverID].user.user.username);
            return false;
        }

        voteKicks[serverID] = {};
        voteKicks[serverID].user = bot.servers[bot.serverFromChannel(channelID)].members[userID];
        voteKicks[serverID].voters = {};
        voteKicks[serverID].warning = false;

        async.each(bot.servers[bot.serverFromChannel(channelID)].members, function(member){
            if(member.status == 'online' && member.user.id != bot.id && member.user.id != voteKicks[serverID].user.user.id)
            {
                voteKicks[serverID].voters[member.user.id] = false;
            }
        });

        setTimeout(function(){
            if(voteKicks[serverID] && !voteKicks[serverID].warning)
            {
                voteKicks[serverID].warning = true;
                m.sendMessage(channelID, '30 seconds left to kick user: ' + voteKicks[serverID].user.user.username);
            }
        }, 30000);

        setTimeout(function(){
            if(voteKicks[serverID])
            {
                m.sendMessage(channelID, 'Failed vote kick for user: ' + voteKicks[serverID].user.user.username);
                delete voteKicks[serverID];
            }
        }, 60000);

        m.sendMessage(channelID, 'Vote kick started, you have 1 minute to vote "yes/y" or "no/n".');
    }
    else
    {
        m.sendMessage(channelID, 'No such user in the channel, or you didn\'t tag the user.');
    }
}

function getVotedUsers(serverID)
{
    count = 0;
    async.each(voteKicks[serverID].voters, function(voter){
        if(voter)
        {
            count = count+1;
        }
    });

    return count;
}

function getYesVotedUsers(serverID)
{
    count = 0;
    async.each(voteKicks[serverID].voters, function(voter){
        if(voter == 'y')
        {
            count = count+1;
        }
    });

    return count;
}

function getNoVotedUsers(serverID)
{
    count = 0;
    async.each(voteKicks[serverID].voters, function(voter){
        if(voter == 'n')
        {
            count = count+1;
        }
    });

    return count;
}

function checkVoteKick(serverID, channelID)
{
    yesVotedPercentage = (getYesVotedUsers(serverID)/Object.keys(voteKicks[serverID].voters).length)*100;
    if(parseFloat(yesVotedPercentage).toFixed(0) > 50)
    {
        m.sendMessage(channelID, 'Majority voted yes, will kick ' + voteKicks[serverID].user.user.username + ' now.');
        bot.kick({
            channel: serverID,
            target: voteKicks[serverID].user.user.id
        });

        m.sendMessage(channelID, "Tried to kick: " + bot.fixMessage(parameters[0]))
        delete voteKicks[serverID];
        return true;
    }

    noVotedPercentage = (getNoVotedUsers(serverID)/Object.keys(voteKicks[serverID].voters).length)*100;
    if(parseFloat(noVotedPercentage).toFixed(0) > 50)
    {
        m.sendMessage(channelID, 'Majority voted no, ' + voteKicks[serverID].user.user.username + ' will not be kicked.');
        delete voteKicks[serverID];
        return false;
    }

    return false;
}


module.exports = {
    command: voteKick,
    voteKicks: voteKicks,
    getVotedUsers: getVotedUsers,
    getYesVotedUsers: getYesVotedUsers,
    getNoVotedUsers: getNoVotedUsers,
    checkVoteKick: checkVoteKick
}