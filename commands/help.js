var botConfig = config.Bot

//help command
function help(user, channelID, parameters, userID)
{
    helpCommand = commands['help'];
    if(parameters.length > 0)
    {
        helpCommand = parameters[0]
        if(!commands.hasOwnProperty(helpCommand))
        {
            m.sendMessage(channelID, 'Command "' + helpCommand + '" doesn\'t exit')
            return false
        }

        helpCommand = commands[helpCommand]
        if(helpCommand.bossOnly)
        {
            m.sendMessage(channelID, 'Command "' + helpCommand + '" doesn\'t exit')
            return false
        }    
    }
    
    
    var returnMessage = 'Instructions for `' + helpCommand.name + '` command \n'
    returnMessage += botConfig.command_prefix + helpCommand.name + " " + helpCommand.instructions
    m.sendMessage(channelID, returnMessage)
}

module.exports = {
    command: help
}