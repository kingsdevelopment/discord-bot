var request = require('request');
var S = require('string');
var async = require('async');
var fs = require('fs');

//search osu api
function osu(user, channelID, parameters, userID)
{
    osuTypes = {'osu' : {id: 0}, 'mania' : {id: 3}, 'taiko' : {id: 1}, 'ctb' : {id: 2}}
    osuconfig = config.osu
    defaultUrl = 'https://osu.ppy.sh/api/{{type}}?k=e805cd79c8e354cfaf21ac09b8ac579acd7ef020'

    gamemode = parameters.length > 2 ? parameters[2] : 'osu'

    if(parameters.length == 1 || parameters[1] == 'player')
    {
        request(defaultUrl.replace('{{type}}', 'get_user') + '&u=' + parameters[0] + '&m=' + osuTypes[gamemode].id + '&type=string', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                data = JSON.parse(body)
                
                if(data.length == 0)
                {
                    return m.sendMessage(channelID, 'No data found for user: ' + parameters[0])
                }

                data = data[0] 

                userData = parameters[0] + ' (lvl. ' + parseFloat(data.level).toFixed(0) + ')\n\n' 

                userData += 'Playcount: ' + data.playcount + ' (' + parseFloat(data.accuracy).toFixed(2) + '% acc)\n'

                userData += 'Ranked Score: ' + data.ranked_score + '\n'

                userData += 'PP: ' + parseFloat(data.pp_raw).toFixed(0) + ' \n'
                userData += 'Rank: #' + data.pp_rank + ' (' + data.country + ': #' + data.pp_country_rank + ')\n\n'

                userData += 'https://osu.ppy.sh/u/' + data.user_id + '\n'
                userData += 'https://a.ppy.sh/' + data.user_id + '\n'

                m.sendMessage(channelID, userData);
            }
        });
    }

    if(parameters[1] == 'recent')
    {
        request(defaultUrl.replace('{{type}}', 'get_user_recent') + '&u=' + parameters[0] + '&m=' + osuTypes[gamemode].id + '&limit=50&type=string', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                data = JSON.parse(body)

                if(data.length == 0)
                {
                    return m.sendMessage(channelID, 'No data found for user: ' + parameters[0])
                }
                    
                var totalCount = 0;
                var returnData = ""
                var count = 1
                var addedIds = []
                var messageSent = false

                async.eachLimit(data, 1, function(song, nextSong){ 
                    if(count < 4)
                    {
                        if(addedIds.indexOf(song.beatmap_id) < 0)
                        {
                            if(song.rank != 'F')
                            {
                                getBeatmapData(osuTypes[gamemode].id, song.beatmap_id, function(beatmapData){
                                    if(beatmapData)
                                    {

                                        returnData += count + '. ' + beatmapData.title + ' by ' + beatmapData.artist + ' (' + song.beatmap_id + ')\n'
                                        returnData += 'Difficulty: ' + beatmapData.version + ' ' + getStars(beatmapData.difficultyrating) + ' (' + beatmapData.bpm + ' bpm)\n'

                                        returnData += 'Score: ' + song.score + '\n'
                                        returnData += 'Max Combo: ' + song.maxcombo + '\n\n'

                                        returnData += song.countgeki + 'x Geki, '
                                        returnData += song.count300 + 'x x300\'s, '
                                        returnData += song.countkatu + ' Katu, '
                                        returnData += song.count100 + 'x 100\'s, '
                                        returnData += song.count50 + 'x 50\'s, '
                                        returnData += song.countmiss + 'x Misses\n\n'

                                        returnData += 'Completion Rank: ' + (song.rank != 'F' ? song.rank : 'Unfinished') + '\n'


                                        if(count < 3)
                                        {
                                            returnData += '\n--------------------------\n\n'
                                        }

                                        if(count < 4)
                                        {
                                            addedIds.push(song.beatmap_id)
                                            count = count+1
                                            nextSong()
                                        }

                                        if((count) == 4)
                                        {
                                            m.sendMessage(channelID, "Last " + (count-1) + " finished played songs by: " + parameters[0] + '\n\n' + returnData);
                                        }
                                    }
                                    else
                                    {
                                        nextSong()
                                    }
                                });
                            }
                            else
                            {
                                nextSong()
                            }
                        }
                        else
                        {
                            nextSong()
                        }

                        totalCount = totalCount+1
                        if(totalCount >= data.length && !messageSent)
                        {
                            m.sendMessage(channelID, "Last " + (count-1) + " finished played songs by: " + parameters[0] + '\n\n' + returnData)
                            messageSent = true
                        }
                    }
                    else
                    {
                        return false;
                    }
                });
            }
        });
    }

    if(parameters[1] == 'top')
    {
        request(defaultUrl.replace('{{type}}', 'get_user_best') + '&u=' + parameters[0] + '&m=' + osuTypes[gamemode].id + '&limit=100&type=string', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                data = JSON.parse(body)

                if(data.length == 0)
                {
                    return m.sendMessage(channelID, 'No data found for user: ' + parameters[0])
                }

                var returnData = parameters[0] + "'s top 3 beatmap scores.\n\n"
                var count = 1
                var addedIds = []

                async.eachLimit(data, 1, function(song, nextSong){
                    if(count < 4)
                    {
                        if(addedIds.indexOf(song.beatmap_id) < 0)
                        {
                            getBeatmapData(osuTypes[gamemode].id, song.beatmap_id, function(beatmapData){
                                if(beatmapData)
                                {
                                    returnData += count + '. ' + beatmapData.title + ' by ' + beatmapData.artist + ' (' + song.beatmap_id + ')\n'
                                    returnData += 'Difficulty: ' + beatmapData.version + ' ' + getStars(beatmapData.difficultyrating) + ' (' + beatmapData.bpm + ' bpm)\n'

                                    returnData += 'Score: ' + song.score + '\n'
                                    returnData += 'Max Combo: ' + song.maxcombo + '\n\n'

                                    returnData += song.countgeki + 'x Geki, '
                                    returnData += song.count300 + 'x x300\'s, '
                                    returnData += song.countkatu + ' Katu, '
                                    returnData += song.count100 + 'x 100\'s, '
                                    returnData += song.count50 + 'x 50\'s, '
                                    returnData += song.countmiss + 'x Misses\n\n'

                                    returnData += 'Rank: ' + (song.rank != 'F' ? song.rank : 'Unfinished') + ' ('+ parseFloat(song.pp).toFixed(0) +' pp gained)\n'
                                    //returnData += 'Mods: ' + 

                                    if(count < 3)
                                    {
                                        returnData += '\n--------------------------\n\n'
                                    }

                                    if(count < 4)
                                    {
                                        addedIds.push(song.beatmap_id)
                                        count = count+1
                                        nextSong()
                                    }

                                    if(count == 4)
                                    {
                                        m.sendMessage(channelID, returnData);
                                    }
                                }
                                else
                                {
                                    nextSong()
                                }
                                
                            });
                        }
                        else
                        {
                            nextSong()
                        }
                    }
                });
            }
        });
    }

    if(parameters[1] == 'replay')
    {
        if(parameters.length < 4)
        {
            m.sendMessage(channelID, "To get a replay, you would need to add a beatmap_id");
        }
        else
        {
            request(defaultUrl.replace('{{type}}', 'get_user') + '&u=' + parameters[0] + '&m=' + osuTypes[gamemode].id + '&type=string', function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    userData = JSON.parse(body)
                    if(userData.length > 0)
                    {
                        userData = userData[0]
                        replayUrl = defaultUrl.replace('{{type}}', 'get_replay') + '&u=' + userData.user_id + '&m=' + osuTypes[gamemode].id + '&b=' + parameters[3];
                        request(replayUrl, function (error, response, body) {
                            data = JSON.parse(body)
                            if(data.hasOwnProperty('error'))
                            {
                                m.sendMessage(channelID, data.error)
                            }
                            else
                            {
                                if(data.hasOwnProperty('content'))
                                {
                                    if(data.content.length > 0)
                                    {
                                        var tempFile = new Buffer(data.content);
                                        var file_name = './temp/' + user + '_' + userID + '_' + parameters[3] + '.osr';

                                        return fs.writeFile(file_name, tempFile, function(err){
                                            if(err)
                                            {
                                                return m.sendMessage(channelID, 'Replay couldn\'t be saved, try again later.');
                                            }

                                            m.sendMessage(channelID, 'Here is your replay, good luck!');
                                            return bot.uploadFile({
                                                channel: channelID,
                                                file: file_name
                                            }, function(response) { //CB Optional
                                                fs.unlink(file_name);
                                            });

                                        });
                                    }
                                }

                                return m.sendMessage(channelID, 'No replay data found..');
                                
                            }
                        });
                    }
                    else
                    {
                        m.sendMessage(channelID, 'User "' + parameters[0] + '" not found.');
                    }
                }
            });

            
        }
    }
}


function getBeatmapData(type, beatmap_id, callback){
    beatmapUrl = defaultUrl.replace('{{type}}', 'get_beatmaps') + '&m=' + type + '&type=string&limit=3&b=' + beatmap_id
    request(beatmapUrl, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            data = JSON.parse(body);

            if(data.length > 0)
            {
                beatmapData = data[0]
            }   
            else
            {
                beatmapData = false;
            }        
        }
        else
        {
            beatmapData = false;
        }

        callback(beatmapData);
    });
}

function getStars(difficultyrating)
{
    stars = '';
    for(i = 0; i < difficultyrating; i++)
    {
        stars += '✫'
    }

    return stars;
}


module.exports = {
    command: osu,
    getBeatmapData: getBeatmapData,
    getStars: getStars
}