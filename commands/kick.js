
var addUserPoints = require('../commands/points').addUserPoints;

//kickuser
function kickUser(user, channelID, parameters, userID)
{
    userid = parameters[0].replace('<@', '').replace('>', '')

    serverid = bot.serverFromChannel(channelID)
    var botserver = bot.servers[serverid]
    var botuser = botserver.members[userid]
    
    if(botuser)
    {
        bot.kick({
            channel: botserver.id,
            target: botuser.user.id
        });

        addUserPoints(bot.id, botuser.user.id, channelID, -10, false, 'kick', function(amount){
            m.sendMessage(channelID, '<@' + botuser.user.id + '> has lost 10 point(s) and is now kicked.')
        }); 

    }
    else
    {
        m.sendMessage(channelID, 'No such user found.');
    }

}

module.exports = {
    command: kickUser
}