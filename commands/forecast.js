var owm = require('openweathermap');
var moment = require('moment');

var owm_config = config.OpenWeatherMap


var emoji = {
    'clear-day' : ':sunrise_over_mountains: ',
    'clear-night' : ':night_with_stars: ',
    'Rain' : ':droplet:',
    'Snow' : ':snowflake:',
    'sleet' : ':droplet:',
    'wind' : ':dash:',
    'fog' : ':foggy: ',
    'Clouds' : ':cloud:',
    'partly-cloudy-day' : ':partly_sunny:',
    'partly-cloudy-night' : ':partly_sunny:'
}

owm.defaults({ang:'en', mode:'json'})
var unitSystem = 'metric'


function getWindDirection(degree, success, error)
{
    val = parseInt((degree/22.5)+.5)
    directions = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]


    if(!directions[(val % 16)])
    {
        if(typeof error == 'function')
        {
            success(false)
            return
        }

        return false
    }

    direction = directions[(val % 16)]

    if(typeof success == 'function')
    {
        success(direction)
        return
    }

    return direction
}

function weather(user, channelID, parameters, userID)
{
    unitSystem = 'metric'
    if(parameters.length == 2)
    {
        if(parameters[1].toLowerCase() != 'metric' && parameters[1].toLowerCase() != 'imperial')
        {
            return m.sendMessage(channelID, "You can only choose between metric and imperial system.")
        }

        unitSystem = parameters[1].toLowerCase()
    }

    owm.find({q: encodeURIComponent(parameters[0]), cnt: 2, appid: owm_config.key, units: unitSystem}, function(err, result){
        if(err != null || result.count <= 0)
        {
            return m.sendMessage(channelID, "No weather data found for: `" + parameters[0] + "`.")
        }

        weather_data = result.list[0]
        if(unitSystem == 'metric')
        {
            temp_degree = 'C'
            wind_speed = 'meter/sec,'
        }
        else if(unitSystem == 'imperial')
        {
            temp_degree = 'F'
            wind_speed = 'miles/hour'
        }

        weather = weather_data.weather[0]
        main = weather_data.main

        getWindDirection(weather_data.wind.deg, function(result){
            windDirection = result

            var message = 'Weather data for: `' + weather_data.name + ' (' + weather_data.sys.country + ')`:\n\n'

            message += '`' + weather.main + '(' + weather.description + ')`\n'
            message += 'Temperature: `' + main.temp + '°' + temp_degree + '`\n'
            message += 'Min temperature: `' + main.temp_min + '°' + temp_degree + '`\n'
            message += 'Max temperature: `' + main.temp_max + '°' + temp_degree + '`\n'
            message += 'Humidity: `' + main.humidity + '%`\n'
            message += 'Wind speed: `' + weather_data.wind.speed + ' ' + wind_speed + ' ' + windDirection + '`\n\n'


            message += weather.iconUrl + '\n'

            m.sendMessage(channelID, message);
        }, function(){
            return m.sendMessage(channelID, "No weather data found for: `" + parameters[0] + "`.")
        });
    });

}

module.exports = {
    weather: weather,
    getWindDirection: getWindDirection
}