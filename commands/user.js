var async = require('async')
var moment = require('moment')

var user = require('../models/user');
var getUserPoints = require('./points.js').getUserPoints

function getUser(userid, cb)
{
    user.find({where: {user_id: userid}}).then(function(user) {
        cb(user)
    })
}


function userInfo(user, channelID, parameters, userID)
{
    botuser = false
    if(parameters.length >= 1)
    {
        user_id = parameters[0].replace(/<@/g, '').replace(/>/g, '')
        if(m.isPrivate(channelID))
        {
            return m.sendMessage(channelID, "You can't look up any other user than yourself in a private message.")
        }

        server_id = bot.serverFromChannel(channelID)
        if(server_id)
        {
            if(bot.servers[server_id].members.hasOwnProperty(user_id))
            {
                botuser = bot.servers[server_id].members[user_id] 
            }
        }
    }
    else
    {
        user_id = userID
        isPrivate = m.isPrivate(channelID)
        if(isPrivate)
        {
            botuser = isPrivate.recipient
        }
        else
        {
            server_id = bot.serverFromChannel(channelID)
            if(server_id)
            {
                if(bot.servers[server_id].members.hasOwnProperty(user_id))
                {
                    botuser = bot.servers[server_id].members[user_id] 
                }
            }
        }
    }


    getUser(user_id, function(dbuser){

        dbuser.getServers().then(function(servers){
            totalServers = []

            async.forEach(servers, function(server){
                totalServers.push(server.name)
            });


            if(m.isPrivate(channelID))
            {
                message = '```\n'

                message += 'Username: ' + dbuser.username   + ' (' + dbuser.user_id + ')\n'
                message += 'Discriminator: ' + botuser.discriminator   + '\n'
                
                message += 'Avatar: https://discordapp.com/api/users/' + user_id + '/avatars/' + botuser.avatar + '.jpg\n'

                message += 'Coins: ' + dbuser.coins + '\n'

                message += 'Server(s): ' + (totalServers.length == 0 ? '-' : totalServers.join(', ')) + '\n'

                message += '```'

                m.sendMessage(channelID, message) 
            }
            else
            {
                getUserPoints(user_id, channelID, function(userPts){

                    joined_at = moment(botuser.joined_at)

                    totalRoles = []
                    async.forEach(botuser.roles, function(role){
                        if(bot.servers[server_id].roles[role])
                        {
                            role = bot.servers[server_id].roles[role]
                            totalRoles.push(role.name)
                        }
                    });

                    dbuser.countMessages({
                        where: {
                            server_id: server_id
                        }
                    }).then(function(messageCount){

                        message = '```\n'

                        message += 'Username: ' + dbuser.username   + ' (' + dbuser.user_id + ') (' + dbuser.id + ')\n'
                        message += 'Discriminator: ' + botuser.user.discriminator   + '\n'
                        
                        message += 'Avatar: https://discordapp.com/api/users/' + user_id + '/avatars/' + botuser.user.avatar + '.jpg\n'

                        message += 'Coins: ' + dbuser.coins + '\n'
                        message += 'Server points: ' + userPts.amount + '\n'
                        message += 'Message count: ' + messageCount + '\n'

                        message += 'Server(s): ' + (totalServers.length == 0 ? '-' : totalServers.join(', ')) + '\n'

                        message += 'Joined at: ' + joined_at.format('dddd DD MMMM YYYY') + ' at ' + joined_at.format('HH:mm') + '\n'
                        message += 'Status: ' + botuser.status + '\n'

                        message += 'Role(s): ' + (totalRoles.length == 0 ? '-' : totalRoles.join(', ')) + '\n'
                    
                        message += '```'

                        m.sendMessage(channelID, message)
                    }); 
                })
            }
        })
        
    })
}

function message_count(user, channelID, parameters, userID)
{
    user_id = userID

    if(parameters.length >= 1)
    {
        user_id = parameters[0].replace(/<@/g, '').replace(/>/, '')
    }

    server_id = bot.serverFromChannel(channelID);
    getUser(user_id, function(dbuser){
         dbuser.countMessages({
            where: {
                server_id: server_id
            }
        }).then(function(messageCount){
            if(user_id == userID)
            {
                return m.sendMessage(channelID, "You have sent " + messageCount + " messages in this server.")
            }
            else
            {
                return m.sendMessage(channelID, parameters[0] + " has sent " + messageCount + " messages in this server.")   
            }
        });
    });
}

module.exports = {
    getUser: getUser,
    userInfo: userInfo,
    message_count: message_count
}