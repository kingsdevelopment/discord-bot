var S = require('string')
var addUserPoints = require('../commands/points').addUserPoints;

// delete messages by bot
function delete_messages(user, channelID, parameters, userID)
{
    bot.getMessages({
        channel: channelID,
        limit: 200 //If 'limit' isn't added, it defaults to 50, the Discord default
    }, function(messageArr) {
        messageArr.forEach(function( value){
            if(value.author.id == bot.id)
            {
                bot.deleteMessage({
                    channel: value.channel_id,
                    messageID: value.id
                });
            }
        });

        m.sendMessage(channelID, 'Deleted all my messages, I am not sure if all of them are gone, but I am pretty sure most of them are!');
    });
}

function purge(user, channelID, parameters, userID)
{
    if(isNaN(parameters[1]))
    {
        return m.sendMessage(channelID, "Amount must be a number.");
    }

    if(parseInt(parameters[1]) > 25)
    {
        return m.sendMessage(channelID, "Amount can't be greater than 25.");
    }

    if(!S(parameters[0]).startsWith('<@'))
    {
        return m.sendMessage(channelID, "Not a valid user, you should mention them.")
    }

    user_id = parameters[0].replace(/<@/g, '').replace(/>/, '')

    bot.getMessages({
        channel: channelID,
        limit: 200 //If 'limit' isn't added, it defaults to 50, the Discord default
    }, function(messageArr) {
        var count = 0;
        messageArr.forEach(function( value){
            if(value.author.id == user_id && count < parseInt(parameters[1]))
            {
                bot.deleteMessage({
                    channel: value.channel_id,
                    messageID: value.id
                });

                count++;
            }
        });

        losePoints = parseInt(parameters[1])*-1
        if(losePoints < -10)
        {
            losePoints = -10
        }

        addUserPoints(bot.id, user_id, channelID, losePoints, false, 'kick', function(amount){
            m.sendMessage(channelID, '<@' + user_id + '> has lost ' + (losePoints*-1) + ' point(s) and ' + parameters[1] + ' of his messages got removed.')
        });
    });
}

module.exports = {
    command: delete_messages,
    purge: purge
}