var moment = require('moment');
var async = require('async');

var isAdmin = require('../libs/roles').isAdmin;

var points = require('../libs/models').points;
var points_log = require('../libs/models').points_log
var server = require('../libs/models').server

function getUserPoints(user_id, channel_id, cb)
{
    server_id = bot.serverFromChannel(channel_id);

    if(!bot.servers.hasOwnProperty(server_id) || !bot.servers[server_id].members.hasOwnProperty(user_id))
    {
        cb(false);
        return false;
    }

    points.findOrCreate({
        where: {
            server_id: server_id,
            user_id: user_id
        }
    }).then(function(pts) {
        if(pts)
        {
            cb(pts[0]);    
        }
        else
        {
            cb(false);
        }
    })
}

function checkPointsSystem(channelID, cb)
{
    server_id = bot.serverFromChannel(channelID)
    server.find({where: {server_id: serverid}}).then(function(server) {
        if(typeof cb == 'function')
        {
            cb(server.enable_points)    
        }
        else
        {
            return server.enable_points
        }
    })
}

function addUserPoints(from, to, channel_id, amount, admin, type, cb, cb2)
{
    server_id = bot.serverFromChannel(channel_id);

    checkPointsSystem(channel_id, function(response){
        if(response)
        {
            getUserPoints(to, channel_id, function(receiver){
                if(!receiver)
                {
                    if(typeof cb2 == 'function')
                    {
                        return cb2(parameters[0] + " was not found in the database yet, maybe he needs to say something?")
                    }
                    else
                    {
                        return false;
                    }
                }

                if(receiver.frozen)
                {
                    if(typeof cb2 == 'function')
                    {
                        return cb2(parameters[0] + "'s points are frozen. type !unfreeze_pts to unfreeze the points.")
                    }
                    else
                    {
                        return false;
                    }
                }
                
                if((parseInt(receiver.amount)+parseInt(amount)) > 2147483647)
                {
                    receiver.amount = 2147483647;
                }
                else
                {
                    receiver.amount = parseInt(receiver.amount)+parseInt(amount);
                }
                

                receiver.save().then(function(){
                    if(!admin && type == 'user' || type != 'user')
                    {
                        points_log.create({
                            user_id: from,
                            receiver_id: to,
                            type: type,
                            server_id: bot.serverFromChannel(channel_id)
                        });
                    }

                    if(typeof cb == 'function')
                    {
                        cb(receiver.amount);
                    }
                });
                
            }); 
        }
        else
        {
            if(typeof cb2 == 'function')
            {
                return cb2("Points system is disabled for this server.")
            }
        }
    });
    
}

function showPoints(user, channelID, parameters, userID)
{
    if(parameters.length >= 1)
    {
        user_id = parameters[0].replace(/<@/g, '').replace(/>/g, '')
        getUserPoints(user_id, channelID, function(pts){
            if(!pts)
            {
                return m.sendMessage(channelID, parameters[0] + " was not found in the database yet, maybe he needs to say something?")
            }

            mainMessage = parameters[0] + " has " + pts.amount + " points.\n"
            
            returnmessage = mainMessage

            if(pts.amount >= 0)
            {
                returnmessage = mainMessage + "He/She must be a newcomer!"
            }

            if(pts.amount >= 20)
            {
                returnmessage = mainMessage + "He/She is becoming more famous!"
            }

            if(pts.amount >= 50)
            {
                returnmessage = mainMessage + "He/She is famous!"
            }

            if(pts.amount >= 1000)
            {
                returnmessage = mainMessage + "He/She is a god!"
            }

            if(pts.amount < 0)
            {
                returnmessage = mainMessage + "Are you hated?";
            }

            m.sendMessage(channelID, returnmessage);
        });
    }
    else
    {
        getUserPoints(userID, channelID, function(pts){
            mainMessage = "You have " + pts.amount + " points.\n"

            if(pts.amount >= 0)
            {
                returnmessage = mainMessage + "You must be a newcomer!"
            }

            if(pts.amount >= 20)
            {
                returnmessage = mainMessage + "You are becoming more famous!"
            }

            if(pts.amount >= 50)
            {
                returnmessage = mainMessage + "You are famous!"
            }

            if(pts.amount >= 100)
            {
                returnmessage = mainMessage + "You totally rigged the votes!"
            }

            if(pts.amount >= 1000)
            {
                returnmessage = mainMessage + "You are a god!"
            }

            if(pts.amount < 0)
            {
                returnmessage = mainMessage + "Are you hated?";
            }


            m.sendMessage(channelID, returnmessage);
        });
    }


}

function acknowledge(user, channelID, parameters, userID)
{
    receiver_name = parameters[0]
    receiver_id = receiver_name.replace(/<@/g, '').replace(/>/g, '')

    userAdmin = isAdmin(user, userID, channelID)

    day_ago = moment.utc().subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss');

    if(userID == receiver_id && !userAdmin)
    {
        return m.sendMessage(channelID, "You can't acknowledge yourself.")
    }

    if(parameters.length >= 2 && parameters[1].length > 10)
    {
        return m.sendMessage(channelID, "Number can't be greater than 2,147,483,647");
    }

    if(parameters.length >= 2 && isNaN(parameters[1]))
    {
        return m.sendMessage(channelID, 'Amount must be an number')
    }
    
    points_log.find({
        where: {
            created: {
                $gte: day_ago
            },
            user_id: userID,
            receiver_id: receiver_id,
            server_id: bot.serverFromChannel(channelID),
            type: 'user'
        }
    }).then(function(log){
        if(!log || userAdmin)
        {
            if(parameters[1])
            {
                if(userAdmin || parameters.length == 3 && parameters[2] == 'custom')
                {
                    amount = parseInt(parameters[1])

                    if(parameters.length == 3 && parameters[2] == 'minus')
                    {
                        amount = amount*-1
                    }
                }
                else
                {
                    return m.sendMessage(channelID, "You sneaky little bastard!")
                }
               
            }
            else
            {
                amount = 1
            }

            addUserPoints(userID, receiver_id, channelID, amount, userAdmin, 'user', function(points){
                m.sendMessage(channelID, receiver_name + " has " + ((amount < 0) ? 'lost' : 'received') + " " + ((amount < 0) ? amount*-1 : amount) + " point(s) and now has " + points + " points.")
            }, function(error){
                m.sendMessage(channelID, error)
            });
        }
        else
        {
            m.sendMessage(channelID, "You can only acknowledge the same user once a day.")
        }
    });
}


function unfreeze_pts(user, channelID, parameters, userID)
{
    user_id = parameters[0].replace(/<@/g, '').replace(/>/g, '')
    getUserPoints(user_id, channelID, function(pts){
        if(pts.frozen)
        {
            pts.frozen = 0;
            pts.save().then(function(){
                m.sendMessage(channelID, parameters[0] + "'s points have been unfrozen.");
            });
        }
        else
        {
            m.sendMessage(channelID, "User's points are not frozen.");
        }
    });
}

function freeze_pts(user, channelID, parameters, userID)
{
    user_id = parameters[0].replace(/<@/g, '').replace(/>/g, '')
    getUserPoints(user_id, channelID, function(pts){
        if(!pts.frozen)
        {
            pts.frozen = 1;
            pts.save().then(function(){
                m.sendMessage(channelID, parameters[0] + "'s points have been frozen.");
            });
        }
        else
        {
            m.sendMessage(channelID, "User's points are already frozen.");
        }
    });
}

function disable_points(user, channelID, parameters, userID)
{
    serverid = bot.serverFromChannel(channelID)
    server.find({where: {server_id: serverid}}).then(function(server) {
        if(server.enable_points)
        {
            server.enable_points = 0
            server.save().then(function(){
                m.sendMessage(channelID, "Disabled points system for this server.")
            })    
        }
        else
        {
            m.sendMessage(channelID, "Point system is already disabled.")
        }
        
    })
}

function enable_points(user, channelID, parameters, userID)
{
    serverid = bot.serverFromChannel(channelID)
    server.find({where: {server_id: serverid}}).then(function(server) {
        if(!server.enable_points)
        {
            server.enable_points = 1
            server.save().then(function(){
                m.sendMessage(channelID, "Enabled points system for this server.")
            })    
        }
        else
        {
            m.sendMessage(channelID, "Point system is already enabled.")
        }
        
    })
}

function top_five(user, channelID, parameters, userID)
{
    points.findAll({
        where: {
            server_id: bot.serverFromChannel(channelID),
            frozen: 0
        }, 
        order: [
            ['amount', 'DESC']
        ],
        limit: 5
    }).then(function(top_points){
        var returnMessage = 'Top ' + top_points.length + ' points:\n'
        
        async.forEachLimit(top_points, 1, function(top, next){
            top.getUser().then(function(user){
                returnMessage += '- `' + user.username + '` with ' + top.amount + ' points.\n'
                next()
            }, function(){
                next()
            });
        }, function(){
            m.sendMessage(channelID, returnMessage);
        });

    });
}

module.exports = {
    showPoints: showPoints,
    acknowledge: acknowledge,
    getUserPoints: getUserPoints,
    addUserPoints: addUserPoints,
    unfreeze_pts: unfreeze_pts,
    freeze_pts: freeze_pts,
    disable_points: disable_points,
    enable_points: enable_points,
    top_five: top_five
}