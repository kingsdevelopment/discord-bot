var nodegit = require("nodegit");
var path = require("path");

var repository;

function update(user, channelID, parameters, userID)
{
    // Open a repository that needs to be fetched and fast-forwarded
    nodegit.Repository.open(path.resolve('./'))
    .then(function(repo) {
        repository = repo;

        return repository.fetchAll({
            credentials: function(url, userName) {
                return nodegit.Cred.sshKeyFromAgent(userName);
            },
            certificateCheck: function() {
                return 1;
            }
        });
    })
    // Now that we're finished fetching, go ahead and merge our local branch
    // with the new one
    .then(function() {
        return repository.mergeBranches("master", "origin/master");
    })
    .done(function() {
        if(parameters.length >= 0 && parameters[0] == 'true')
        {
            Object.keys(bot.servers).forEach(function(server){
                m.sendMessage(server, "Going for a reboot! Be right back!")
            })    
        }
        
        process.exit()
    });
}


module.exports = {
    command: update
}

